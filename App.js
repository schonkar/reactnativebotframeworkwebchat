import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import ReactWebChat, { createDirectLine } from 'botframework-webchat';

export default function App() {
  return (
    <View style={styles.container}>
      <Text>Open up App.js to start working on your applets!</Text>
      <StatusBar style="auto" />
      <ReactWebChat directLine={directLine} userID="sz" />;
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
const directLine = createDirectLine({ token: process.env.DLSECRET })
